sap.ui.define([
	"sap/m/Button",
	"sap/ui/core/mvc/Controller",
	"sap/m/Dialog",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function(Button, Controller, Dialog, JSONModel, MessageBox) {
	"use strict";
	var bewertungenModel = null;
	var kondition = null;
	var konditionenModel = null;
	var objekteModel = null;
	var partner = null;
	return Controller.extend("csc.kreditantrag.controller.Kreditantrag", {
		onInit: function() {},
		bonitaetValidation: function() {
			var wizard = this.getView().byId("KreditantragWizard");
			this.getView().byId("BonitaetTable").setModel(this.getBewertungenModel());
			this.getView().byId("BonitaetAntragstellerOrt").setValue(this.getPartner().Name2 + " " + this.getPartner().Name1 + ", " +
				this.getPartner().City);
			this.getView().byId("BonitaetBeruf").setValue(this.getPartner().Beruf);
			this.getView().byId("BonitaetBranche").setValue(this.getPartner().Branche);
			this.getView().byId("BonitaetAngestelltSelbstaendig").setValue(this.getPartner().AngestelltSelbststaendig);
			this.getView().byId("BonitaetSeit").setValue(this.getPartner().AngestelltSelbststaendig_Seit);
			this.getView().byId("BonitaetEinnahmen").setValue(this.numberFormat(this.getPartner().EinnahmenPA));
			this.getView().byId("BonitaetAusgaben").setValue(this.numberFormat(this.getPartner().AusgabenPA));
			this.getView().byId("BonitaetBruttojahresgehalt").setValue(this.numberFormat(this.getPartner().Bruttojahresgehalt));
			this.getView().byId("BonitaetAusgabenZinsTilgung").setValue(this.numberFormat(this.getPartner().AusgabenZinsTilgung));
			this.getView().byId("BonitaetEinkuenfteGewerbe").setValue(this.numberFormat(this.getPartner().EinkuenfteGewerbe));
			this.getView().byId("BonitaetAusgabenWohnen").setValue(this.numberFormat(this.getPartner().AusgabenWohnen));
			this.getView().byId("BonitaetEinkuenfteVermietungVerpachtung").setValue(this.numberFormat(this.getPartner().EinkuenfteVermietungVerpachtung));
			this.getView().byId("BonitaetAusgabenUnterhalt").setValue(this.numberFormat(this.getPartner().AusgabenUnterhalt));
			this.getView().byId("BonitaetEinkuenfteKapitalvermoegen").setValue(this.numberFormat(this.getPartner().EinkuenfteKapital));
			this.getView().byId("BonitaetAusgabenSonstige").setValue(this.numberFormat(this.getPartner().AusgabenSonst));
			this.getView().byId("BonitaetEinkuenfteSonstige").setValue(this.numberFormat(this.getPartner().EinkuenfteSonst));
			wizard.validateStep(this.getView().byId("bonitaetStep"));
			setTimeout(function() {
				$("#__xmlview0--KreditantragWizard-nextButton-content").html("Konditionen");
			}, 0);
		},
		dateFormat: function(dateString) {
			var date = new Date(dateString);
			var dd = date.getDate().toString();
			var mm = (date.getMonth() + 1).toString();
			var yyyy = date.getFullYear().toString();
			return (dd[1] ? dd : "0" + dd[0]) + "." + (mm[1] ? mm : "0" + mm[0]) + "." + yyyy;
		},
		finanzierungsdatenValidation: function() {
			var ctrl = this;
			var partnerId = location.hash.substring(location.hash.lastIndexOf("/") + 1);
			var wizard = this.getView().byId("KreditantragWizard");
			this.setPartner(partnerId);
			this.getView().byId("FinanzierungsdatenForm").setModel(this.getObjekteModel());
			this.getView().byId("AntragstellerOrt").setValue(this.getPartner().Name2 + " " + this.getPartner().Name1 + ", " + this.getPartner()
				.City);
			this.getView().byId("Darlehensbetrag").addEventDelegate({
				onfocusout: function(event) {
					ctrl.getView().byId("Darlehensbetrag").setValue(ctrl.numberFormat(ctrl.getView().byId("Darlehensbetrag").getValue()));
				}
			});
			this.getView().byId("zDarlehenswunsch").setValue(this.getView().byId("Darlehensbetrag").getValue());
			this.getView().byId("zZinsbeginn").setValue(this.getView().byId("Zinsbeginn").getValue());
			this.getView().byId("zTilgungsbeginn").setValue(this.getView().byId("Tilgungsbeginn").getValue());
			if (this.validateInputs(["KaufpreisImmobilie", "Darlehensbetrag", "Zinsbeginn", "Tilgungsbeginn"])) {
				wizard.validateStep(this.getView().byId("FinanzierungsdatenStep"));
				setTimeout(function() {
					$("#__xmlview0--KreditantragWizard-nextButton-content").html("Bonität");
				}, 0);
			} else {
				wizard.invalidateStep(this.getView().byId("FinanzierungsdatenStep"));
			}
		},
		getBewertungenModel: function() {
			if (bewertungenModel === null) {
				var bewArray = [];
				var request = $.ajax({
					async: false,
					data: {},
					dataType: "json",
					type: "GET",
					success: function() {},
					url: "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/BewertungenSet"
				});
				if (request.readyState === 4 && request.status === 200) {
					bewArray = request.responseJSON.d.results;
					for (var ii = 0; ii < bewArray.length; ii++) {
						bewArray[ii].bewertungsmethode = bewArray[ii].Bewertungsmethode;
						bewArray[ii].bonitaet = "-" + this.numberFormat(bewArray[ii].Bonitaet);
						bewArray[ii].ratingergebnis = bewArray[ii].Ratingergebnis;
						bewArray[ii].tendenz = bewArray[ii].Tendenz;
					}
				}
				bewertungenModel = new JSONModel(bewArray);
			}
			return bewertungenModel;
		},
		getFestschreibungsende: function() {
			var lz = parseInt(kondition.laufzeit);
			var zb = this.dateFormat(this.getView().byId("Zinsbeginn").getValue());
			var zbYear = parseInt(zb.substring(zb.lastIndexOf(".") + 1));
			var fseYear = zbYear + lz;
			var fse = zb.substring(0, zb.lastIndexOf(".") + 1) + (fseYear.toString()[1] ? fseYear : "0" + fseYear.toString()[0]);
			fse = this.dateFormat(fse);
			return fse;
		},
		getKonditionenModel: function() {
			if (konditionenModel === null) {
				var kondArray = [];
				var request = $.ajax({
					async: false,
					data: {},
					dataType: "json",
					type: "GET",
					success: function() {},
					url: "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/KonditionenSet"
				});
				if (request.readyState === 4 && request.status === 200) {
					kondArray = request.responseJSON.d.results;
					for (var ii = 0; ii < kondArray.length; ii++) {
						kondArray[ii].laufzeit = parseInt(kondArray[ii].laufzeit);
						kondArray[ii].zinssatz = this.numberFormat(kondArray[ii].zinssatz);
						kondArray[ii].tilgung = this.numberFormat(kondArray[ii].tilgung);
					}
				}
				konditionenModel = new JSONModel(kondArray);
			}
			return konditionenModel;
		},
		getMonthlyRate: function() {
			var db = this.numberRaw(this.getView().byId("Darlehensbetrag").getValue());
			var tg = parseFloat(this.numberRaw(kondition.tilgung)) / 100;
			var zs = parseFloat(this.numberRaw(kondition.zinssatz)) / 100;
			var mr = (db * (zs + tg)) / 12;
			return mr;
		},
		getObjekteModel: function() {
			if (objekteModel === null) {
				var objObject = {
					" ": {
						"objekt": " ",
						"objektart": "",
						"kaufpreis": "",
						"strasse": "",
						"hausnummer": "",
						"plz": "",
						"ort": "",
						"_displayText": ""
					}
				};
				var request = $.ajax({
					async: false,
					data: {},
					dataType: "json",
					type: "GET",
					success: function() {},
					url: "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/ObjekteSet"
				});
				if (request.readyState === 4 && request.status === 200) {
					var objArray = request.responseJSON.d.results;
					for (var ii = 0; ii < objArray.length; ii++) {
						objObject[objArray[ii].objekt] = {
							"hausnummer": objArray[ii].hausnummer,
							"kaufpreis": parseFloat(objArray[ii].kaufpreis),
							"objekt": objArray[ii].objekt,
							"objektart": objArray[ii].objektart,
							"ort": objArray[ii].ort,
							"plz": objArray[ii].plz,
							"strasse": objArray[ii].strasse,
							"_displayText": objArray[ii].objekt + ", " + objArray[ii].strasse + " " + objArray[ii].hausnummer + " (" + objArray[ii].objektart +
								")"
						};
					}
				}
				objekteModel = new JSONModel(objObject);
			}
			return objekteModel;
		},
		getPangVo: function() {
			var pangVo = this.numberRaw(kondition.zinssatz);
			pangVo += Math.floor(Math.random() * 7 + 13) / 100;
			return this.numberFormat(pangVo) + " %";
		},
		getPartner: function() {
			return partner;
		},
		getYYYYMMDDDate: function() {
			var date = new Date();
			var yyyy = date.getFullYear().toString();
			var mm = (date.getMonth() + 1).toString();
			var dd = date.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);
		},
		numberFormat: function(numberString) {
			if (numberString === "") {
				return "";
			} else if (numberString.toString().indexOf(",") > -1) {
				return numberString;
			}
			numberString = parseFloat(numberString).toFixed(2);
			return numberString.toString().replace(".", ",").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		},
		numberRaw: function(numberFormatted) {
			var numberRaw = parseFloat(numberFormatted.replace(/\./g, "").replace(",", "."));
			return numberRaw;
		},
		onFinanzierungsobjektChanged: function() {
			var finanzierungsobjektSelect = this.getView().byId("Finanzierungsobjekt");
			var objektartSelect = this.getView().byId("Objektart");
			var kaufpreisImmobilieInput = this.getView().byId("KaufpreisImmobilie");
			var strasseHausnummerImmobilieInput = this.getView().byId("StrasseHausnummerImmobilie");
			var plzImmobilieInput = this.getView().byId("PLZImmobilie");
			var ortImmobilieInput = this.getView().byId("OrtImmobilie");
			var objekt = this.getObjekteModel().getObject("/" + finanzierungsobjektSelect.getSelectedKey());
			objektartSelect.setSelectedKey(objekt.objektart);
			kaufpreisImmobilieInput.setValue(this.numberFormat(objekt.kaufpreis));
			strasseHausnummerImmobilieInput.setValue(objekt.strasse + " " + objekt.hausnummer);
			plzImmobilieInput.setValue(objekt.plz);
			ortImmobilieInput.setValue(objekt.ort);
			this.getView().byId("zObjekt").setValue(objekt.ort + ", " + objekt.strasse + " " + objekt.hausnummer + " (" + objekt.objektart + ")");
			this.finanzierungsdatenValidation();
		},
		onKonditionenInit: function() {
			var wizard = this.getView().byId("KreditantragWizard");
			this.getView().byId("KonditionenTable").setModel(this.getKonditionenModel());
			if (kondition === null) {
				wizard.invalidateStep(this.getView().byId("konditionenStep"));
			} else {
				wizard.validateStep(this.getView().byId("konditionenStep"));
				setTimeout(function() {
					$("#__xmlview0--KreditantragWizard-nextButton-content").html("Zusammenfassung");
				}, 0);
			}
		},
		onKonditionSelect: function(event) {
			var item = event.getParameter("listItem") || event.getSource();
			var path = item.getBindingContext().getPath();
			var rows = $("#__xmlview0--KonditionenTable-listUl").find(".sapMLIB.sapMListTblRow");
			for (var ii = 0; ii < rows.length; ii++) {
				$(rows[ii]).removeClass("itemSelected");
			}
			$("#" + item.getId()).addClass("itemSelected");
			kondition = this.getView().byId("KonditionenTable").getModel().getObject(path);
			this.getView().byId("zZahlungsrhythmus").setValue(kondition.zahlungsrhythmus);
			this.getView().byId("zZinssatz").setValue(kondition.zinssatz + " %");
			this.getView().byId("zTilgungssatz").setValue(kondition.tilgung + " %");
			this.getView().byId("zFestschreibungsende").setValue(this.getFestschreibungsende());
			this.getView().byId("zMonatlicheRate").setValue(this.numberFormat(this.getMonthlyRate()));
			this.getView().byId("zPangVo").setValue(this.getPangVo());
			this.onKonditionenInit();
		},
		onKreditantragComplete: function() {
			var ctrl = this;
			var darlehensbetrag = this.getView().byId("zDarlehenswunsch").getValue();
			var festschreibungsende = this.dateFormat(this.getView().byId("zFestschreibungsende").getValue());
			var objekt = this.getObjekteModel().getObject("/" + this.getView().byId("Finanzierungsobjekt").getSelectedKey());
			var pangVo = this.getView().byId("zPangVo").getValue();
			var tilgungsbeginn = this.dateFormat(this.getView().byId("zTilgungsbeginn").getValue());
			var wizard = this.getView().byId("KreditantragWizard");
			var zinsbeginn = this.dateFormat(this.getView().byId("zZinsbeginn").getValue());
			var xhr = new XMLHttpRequest();
			xhr.open("GET", "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/ContractSet", true);
			xhr.setRequestHeader("x-csrf-token", "fetch");
			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4) {
					var xct = xhr.getResponseHeader("x-csrf-token");
					var postXhr = new XMLHttpRequest();
					postXhr.open("POST", "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/ContractSet", true);
					postXhr.setRequestHeader("accept", "application/json");
					postXhr.setRequestHeader("content-type", "application/json");
					postXhr.setRequestHeader("OData-Version", "2.0");
					postXhr.setRequestHeader("x-csrf-token", xct);
					postXhr.onreadystatechange = function() {
						if (postXhr.readyState === 4) {
							MessageBox.success("Vertrag für " + partner.Name1 + " " + partner.Name2 + " (" + partner.PartnerID +
								") wurde erfolgreich angelegt!");
							wizard.invalidateStep(ctrl.getView().byId("zusammenfassungStep"));
						}
					};
					postXhr.send(JSON.stringify({
						"PartnerID": partner.PartnerID,
						"ContractType": "Darlehen",
						"Object": objekt.ort,
						"Interestrate": ctrl.numberRaw(kondition.zinssatz).toString(),
						"ContractStartDate": "00.00.0000",
						"FixedInterestRateEnd": "00.00.0000",
						"Status": "neu",
						"Zahlungsrhythmus": kondition.zahlungsrhythmus,
						"PangVo": ctrl.numberRaw(pangVo).toString(),
						"Tilgungsbeginn": tilgungsbeginn,
						"Zinsbeginn": zinsbeginn,
						"Darlehensbetrag": ctrl.numberRaw(darlehensbetrag).toString(),
						"Repayment": "0.00",
						"MonatlicheRate": ctrl.getMonthlyRate().toString(),
						"Festschreibungsende": festschreibungsende
					}));
				}
			};
			xhr.send(null);
		},
		/**
		 *@memberOf csc.kreditantrag.controller.Partner
		 */
		onNavBack: function() {
			window.history.go(-1);
		},
		onZusammenfassungInit: function() {
			var objekt = this.getObjekteModel().getObject("/" + this.getView().byId("Finanzierungsobjekt").getSelectedKey());
			var wizard = this.getView().byId("KreditantragWizard");
			if (this.getView().byId("zAntragsnummer").getValue() === "") {
				this.getView().byId("zAntragsnummer").setValue(Math.floor(Math.random() * 90000000 + 10000000));
			}
			this.getView().byId("zAntragsdatum").setValue(this.getYYYYMMDDDate());
			this.getView().byId("zKunde").setValue(this.getPartner().Name2 + " " + this.getPartner().Name1 + ", " + this.getPartner().City +
				", " + this.getPartner().Street + " " + this.getPartner().HouseNumber);
			this.getView().byId("zObjekt").setValue(objekt.ort + ", " + objekt.strasse + " " + objekt.hausnummer + " (" + objekt.objektart +
				")");
			this.getView().byId("zDarlehenswunsch").setValue(this.getView().byId("Darlehensbetrag").getValue());
			this.getView().byId("zMonatlicheRate").setValue(this.numberFormat(this.getMonthlyRate()));
			this.getView().byId("zZinsbeginn").setValue(this.getView().byId("Zinsbeginn").getValue());
			this.getView().byId("zFestschreibungsende").setValue(this.getFestschreibungsende());
			this.getView().byId("zTilgungsbeginn").setValue(this.getView().byId("Tilgungsbeginn").getValue());
			this.getView().byId("zZahlungsrhythmus").setValue(kondition.zahlungsrhythmus);
			this.getView().byId("zZinssatz").setValue(kondition.zinssatz + " %");
			this.getView().byId("zTilgungssatz").setValue(kondition.tilgung + " %");
			this.getView().byId("zPangVo").setValue(this.getPangVo());
			this.getView().byId("zIBAN").setValue(this.getPartner().IBAN);
			this.getView().byId("zBank").setValue(this.getPartner().Bankname);
			wizard.validateStep(this.getView().byId("zusammenfassungStep"));
			setTimeout(function() {
				$("#__xmlview0--KreditantragWizard-nextButton-content").html("Antrag stellen");
			}, 2);
		},
		openObjektDetails: function(event) {
			var dialog = new Dialog({
				afterClose: function() {
					dialog.destroy();
				},
				beginButton: new Button({
					press: function() {
						dialog.close();
					},
					text: "Schließen"
				}),
				modal: true,
				title: "Detailansicht von Objektdaten"
			});
			this.getView().addDependent(dialog);
			dialog.open();
		},
		setPartner: function(partnerId) {
			if (partner === null) {
				var request = $.ajax({
					async: false,
					data: {},
					dataType: "json",
					type: "GET",
					success: function() {},
					url: "/sap/opu/odata/sap/Z_API_KREDITANTRAG_SRV/PartnerSet('" + partnerId + "')"
				});
				if (request.readyState === 4 && request.status === 200) {
					partner = request.responseJSON.d;
				}
				if (typeof partner === "undefined") {
					partner = {
						PartnerID: "",
						PartnerFunction: "",
						CountryCode: "",
						CountryDescription: "",
						CellPhoneNumber: "",
						Email: "",
						TelephoneNumberExtension: "",
						TelephoneNumber: "",
						City: "",
						PostalCode: "",
						HouseNumber: "",
						Street: "",
						Name1: "",
						Name2: "",
						PartnerNumber: "",
						QuotationID: "",
						PartnerFunctionDescription: "",
						PartnerFunctionType: "",
						DateOfBirth: "",
						Beruf: "",
						AngestelltSelbstständig: "",
						EinnahmenPA: "",
						Bruttojahresgehalt: "",
						EinkünfteGewerbe: "",
						EinkünfteVermietungVerpachtung: "",
						EinkünfteKapital: "",
						EinkünfteSonst: "",
						Branche: "",
						BrancheSeit: "",
						AusgabenPA: "",
						AusgabenZinsTilgung: "",
						AusgabenWohnen: "",
						AusgabenUnterhalt: "",
						AusgabenSonst: "",
						Bankname: "",
						IBAN: ""
					};
				}
			}
		},
		validateInputs: function(inputIds) {
			var allOk = null;
			for (var ii = 0; ii < inputIds.length; ii++) {
				if (allOk === null) {
					allOk = this.getView().byId(inputIds[ii]).getValue().length > 0;
				} else {
					allOk = allOk && this.getView().byId(inputIds[ii]).getValue().length > 0;
				}
			}
			return allOk;
		}
	});
});