sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller) {
	"use strict";

	return Controller.extend("csc.kreditantrag.controller.App", {
		gotoKundenbetreuerApp: function() {
			window.location.href = "https://webidetesting2996354-p1941973808trial.dispatcher.hanatrial.ondemand.com/webapp/extended_runnable_file.html";
		}
	});
});